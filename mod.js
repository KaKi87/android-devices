const
    axios = require('axios'),
    iconv = require('iconv-lite'),
    csv = require('neat-csv'),
    capitalize = string => `${string[0].toUpperCase()}${string.slice(1)}`,
    devices = {};

module.exports = {
    refresh: async ({
        sources = ['google', 'xiaomi']
    } = {}) => {
        for(const source of sources){
            switch(source){
                case 'google': {
                    const data = await csv(
                        iconv.decode(
                            (await axios(
                                'https://storage.googleapis.com/play_public/supported_devices.csv',
                                { responseType: 'arraybuffer' }
                            )).data,
                            'utf-16le'
                        )
                    );
                    for(const {
                        'Device': codename,
                        'Retail Branding': brand,
                        'Marketing Name': model1,
                        'Model': model2
                    } of data){
                        if(!devices[codename])
                            devices[codename] = {};
                        devices[codename][source] = {
                            brand,
                            model: model1 === model2 ? model1 : `${model1} ${model2}`
                        };
                    }
                    break;
                }
                case 'xiaomi': {
                    const
                        brand = capitalize(source),
                        { length: brandLength } = brand;
                    let data;
                    ({ data } = await axios('https://xiaomifirmwareupdater.com/data/names.yml'));
                    data = data.split('\n').slice(0, -1);
                    for(const line of data){
                        const [codename, model] = line.split(': ');
                        if(!devices[codename])
                            devices[codename] = {};
                        devices[codename][source] = {
                            brand,
                            model: model.startsWith(brand) ? model.slice(brandLength + 1) : model
                        };
                    }
                    break;
                }
            }
        }
    },
    getDevices: () => Object
        .entries(devices)
        .flatMap(([
                codename,
                sources
            ]) => Object
                .entries(sources)
                .map(([
                    source,
                    device
                ]) => ({
                    codename,
                    source,
                    ...device
                }))
        ),
    getDevice: ({ codename }) => devices[codename]
};